docker compose up --build -d redpanda
docker compose exec redpanda rpk topic create -c cleanup.policy=compact -r 1 -p 1 deviceAdditionTopic deviceConfigTopic deviceMappedTopic deviceRemovalTopic userRegisteredTopic
docker compose exec redpanda rpk topic create -c cleanup.policy=delete -r 1 -p 1 deviceSensorAlertTopic deviceSensorTopic emailsTopic
docker compose down