# IoT Platform demo

## Description

This is a repository which contains all services as git submodules. 
You're able to launch demo of the platform from this repository.

## Starting

1. `mkdir -p docker/volumes/kafka docker/volumes/zookeeper && sudo chown -R 1001:1001 docker/volumes/kafka && sudo chown -R 1001:1001 docker/volumes/zookeeper` - Create directories for `kafka` and `zookeper` volumes and set up correct owner user id.
2. `cp .env.example .env` - Copy `.env.example` into `.env` and configure the enviroment variables.
3. `git submodule init` - Initializes git submodules
4. `git submodule update` - Updates git submodules.
5. `docker-compose up -d --build` - Launches the containers.
6. Open in browser `localhost:81`

## Exposed ports
* `80` - customer react ui.
* `81` - user-authenticator service.
* `82` - device connector
* `83` - device emulator
* `84` - user-devices-rest service
* `85` - email assets
* `86` - user-devices-ws service
* `8080` - adminer.
* `8081` - redpanda console.
* `8082` - mailhog.